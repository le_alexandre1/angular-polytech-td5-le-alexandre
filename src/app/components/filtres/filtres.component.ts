import { Component, EventEmitter, LOCALE_ID, Output, ViewEncapsulation } from '@angular/core';
import { MAT_DATE_RANGE_SELECTION_STRATEGY, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AEROPORTS } from './../../constants/aeroport.constant';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { IAeroport } from '../../models/aeroport.model';
import { ThreeDayRangeSelectionStrategy } from '../../date-adapter';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import {MatCommonModule} from '@angular/material/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { IFiltres } from '../../models/filtres.model';

@Component({
  selector: 'app-filtres',
  standalone: true,
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  imports: [MatIconModule, MatButtonModule, MatInputModule,
    MatFormFieldModule, MatSelectModule, MatDatepickerModule, MatCommonModule, CommonModule],
  providers: [
    provideNativeDateAdapter(),
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: ThreeDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent {

  @Output() selectedFilterEmitter : EventEmitter<IFiltres> = new EventEmitter<IFiltres>();
  isValid : boolean = true;
  filter = {} as IFiltres;

  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */
  aeroports: IAeroport[] = AEROPORTS;
  
  getAirport(airport : IAeroport) {
    this.filter.aeroport = airport;
    if ((this.filter.debut && this.filter.fin)) {
      this.isValid = false;
    }
  }

  getStartDate(startDate: Date | null) {
    this.filter.debut = startDate!;
    if ((this.filter.aeroport && this.filter.fin)) {
      this.isValid = false;
    }
  }

  getEndDate (endDate : Date | null) {
    this.filter.fin = endDate!;
    if ((this.filter.aeroport && this.filter.debut)) {
      this.isValid = false;
    }
  }

  emitFilter() {
    this.selectedFilterEmitter.emit(this.filter);
  }
}
