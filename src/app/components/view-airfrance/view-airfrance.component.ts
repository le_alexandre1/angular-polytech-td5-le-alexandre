import { Component } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { CommonModule } from '@angular/common';
import { IAeroport } from '../../models/aeroport.model';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { IFiltres } from '../../models/filtres.model';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent, CommonModule],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {
  selectedAirport !: IAeroport;
  selectedStartDate !: Date | null;
  selectedEndDate !: Date | null;
  selectedFilterView !: IFiltres;

  getSelectedAirportFromFilter(airport : IAeroport) {
    this.selectedAirport = airport;
  }

  getSelectedStartDateFromFilter (startDate : Date | null) {
    this.selectedStartDate = startDate;
  }

  getSelectedEndDateFromFilter (endDate : Date | null) {
    this.selectedEndDate = endDate;
  }

  getSelectedFilter(filter : IFiltres) {
    this.selectedFilterView = filter;
  }
}
